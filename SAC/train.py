#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Uses build.py methods to train neural nets in SAC

@author: Joe Doerr
"""

import sys
import os
sys.path.append("../")
currentdir = os.path.dirname(os.path.realpath(__file__)) #macro folder
parentdir = os.path.dirname(currentdir) #example folder

import tensorflow as tf
import time

#other file includes
from prop_layers import nn_model
from SAC.run import concatenate_state_action_Q, sample_action_with_entropy
from tflog import Make_summary_info
from SAC_Experimental.hyperparameters import *

learning_rate=tf.Variable(LEARNING_RATE,trainable=False,name='learning_rate') #0.0001 was before
optimizer = tf.keras.optimizers.Adam(learning_rate)
log_path=currentdir+'/'+'logs'
writer = tf.summary.create_file_writer(log_path)

#send to training_instance_anybs to then call train_real_models which will apply updates
def training_instance_anybs(Q_model_list, target_Q_list, policy_model, replay_dataset, update_iters:int, batch_size:int, step, scale_to_fit:int = 10):
  """
  We divide batch size by 10 so that we can put a large batch size in and still can fit on the gpu
  We run the batch sizes from the whole train_dataset and sum them up until 10 and apply each time

  For Q networks:
  for each minibatch calculate TD error-> get_TD_target, pass into train_real_Qs to get gradient for update
  At end average gradients by scale_to_fit, apply to network
  For policy:
  for each minibatch, maximize expected value of Q and entropy with train_policy, get gradients for update
  At end average gradients by scale_to_fit, apply to network
  """
  num_models = 3
  use_policy = num_models-1 # at use_policy num_models iteration, use the policy
  batch_size /= scale_to_fit
  replay_dataset = replay_dataset.batch(int(batch_size))
  for mm in range(num_models):
    #print("model: ", mm)
    #setup
    updates=0
    sum_batch_num = 0
    grads = []
    TD_target_tensor = -5
    avl = 0.0
    ups = 0.0

    for minibatch in replay_dataset:
      #-Get TD Target-#
      TD_target_tensor = get_TD_target(target_Q_list, policy_model, minibatch) #ensures only runs once for both Qs
      #---------------#

      #-Which model used-#
      model = policy_model # Which model to run this for
      if mm < use_policy: model = Q_model_list[mm]
      #------------------#
      #-Getting one minibatch gradient-#
      if sum_batch_num == 0: #~~~start the grads = 
        if mm < use_policy: grads, loss = train_real_Qs(TD_target_tensor, model, minibatch, batch_size) # Q
        else:                          grads, loss = train_policy(policy_model, Q_model_list, minibatch, batch_size) # Policy
      else:                  #~~~work on the exist grads, grads_add = 
        if mm < use_policy: grads_add, loss = train_real_Qs(TD_target_tensor, model, minibatch, batch_size) # Q
        else:                          grads_add, loss = train_policy(model, Q_model_list, minibatch, batch_size) # Policy
        for sg in range(len(grads)): #for each tensor in the list, tf.add them together
          grads[sg] = tf.add(grads[sg], grads_add[sg])
      sum_batch_num += 1
      avl+=tf.reduce_mean(loss) #for tensorboard
      ups+=1.0  #for tensorboard
      #--------------------------------#
      #-Averaging minibatch gradients when "scale_to_fit" number of minibatches run, then applies averaged gradient-#
      if sum_batch_num == scale_to_fit: # ~ When completed the full batch size, make the gradients the average
        sum_batch_num = 0
        divide_by = tf.constant([float(scale_to_fit)], dtype=tf.float64)
        for sg in range(len(grads)): #for each tensor in the list, tf.divide them by the number of grads added together
          grads[sg] = tf.divide(grads[sg], divide_by)

          if tf.reduce_any(tf.math.is_nan(grads[sg])) == True:
            print("gradient nan")
        optimizer.apply_gradients(zip(grads, model.trainable_variables))
        updates+=1
        if updates >= update_iters: break
      #-------------------------------------------------------------------------------------------------------------#

    #tensorboard
    if mm == 0:
      Make_summary_info({'Q_0_loss':avl/float(ups)}, writer, step)
    elif mm == 1:
      Make_summary_info({'Q_1_loss':avl/float(ups)}, writer, step)
    elif mm == use_policy:
      Make_summary_info({'Policy_loss':avl/float(ups)}, writer, step)
  return step+1
  """
  training complete
  """

#Never make this @tf.function, will cause issues with the updating of target networks
def get_TD_target(target_Q_list, policy_model, minibatch):
  _, _, reward, n_states_tensor, d = minibatch
  #sample action from current policy using its normal decision making functionality
  actions_tensor, entropy_tensor = sample_action_with_entropy(policy_model, n_states_tensor)
  input = concatenate_state_action_Q(n_states_tensor, actions_tensor)

  #get lower target Q function for each s, a, using tf.math.minimum between the two output tensors
  tQ_out_0 = target_Q_list[0](input)
  tQ_out_1 = target_Q_list[1](input)
  TD_target = tf.math.minimum(tQ_out_0, tQ_out_1)

  TD_target = TD_target + (ALPHA * entropy_tensor)

  #scale using gamma and d
  TD_target = tf.cast(TD_target, dtype=tf.float64) * (GAMMA * (1.0-d))
  TD_target = TD_target + reward
  return TD_target

@tf.function
def train_real_Qs(TD_target, Q_model, minibatch, batch_size):
  with tf.GradientTape() as tape:
    s, a, _, _, _ = minibatch
    out_tensor = Q_model(concatenate_state_action_Q(s, a)) # ~ model can take tensor as input and output tensor of [batch size, output vector]
    # MSBE between two tensors that have each element as a datapoint, average the losses together then use as minimization target
    loss = tf.math.square(TD_target - tf.cast(out_tensor, dtype=tf.float64))
    loss /= batch_size
    #print("Q_loss: ", tf.reduce_sum(tf.abs(loss)))
    grads = tape.gradient(target=loss, sources=Q_model.trainable_variables)
    return grads, loss

@tf.function
def train_policy(policy_model, Q_model_list, minibatch, batch_size):
  with tf.GradientTape() as tape:
    states_tensor, _, _, _, _ = minibatch #tensor of: [batchsize, statetensorsize] for states_tensor
    actions_tensor, entropy_tensor = sample_action_with_entropy(policy_model, states_tensor)
    input = concatenate_state_action_Q(states_tensor, actions_tensor) #concatenate with states_tensor for input to Q functions

    #apply to both Q_models then tf.math.minimum to find the minimum for each datapoint
    Q_out_0 = Q_model_list[0](input)
    Q_out_1 = Q_model_list[1](input)
    loss = tf.math.minimum(Q_out_0, Q_out_1)

    loss = loss + (ALPHA * entropy_tensor) #entropy can be negative when there are super low stds
    loss = loss / batch_size

    # we want to perform gradient ascent but the optimizer always minimizes, make negative to achieve
    loss *= -1.0
    grads = tape.gradient(target=loss, sources=policy_model.trainable_variables)

    # gradients will move mean towards best expected value given all the actions it took given the std all the noise it has sampled
    # std will move towards best expected value given all actions it took given the std over all the noise it has sampled, entropy term keeps std higher
    return grads, loss

def train_target_models(Q_model_list, target_Q_list):
  #for loop to each layer we scale the tensors and add them
  for mm in range(len(Q_model_list)):
    for dl in range(len(Q_model_list[mm].dense_layers)):
      target_Q_list[mm].dense_layers[dl].w = (target_Q_list[mm].dense_layers[dl].w * POLYAK_CONSTANT) + (Q_model_list[mm].dense_layers[dl].w * (1.0-POLYAK_CONSTANT))
      target_Q_list[mm].dense_layers[dl].b = (target_Q_list[mm].dense_layers[dl].b * POLYAK_CONSTANT) + (Q_model_list[mm].dense_layers[dl].b * (1.0-POLYAK_CONSTANT))
    target_Q_list[mm].output_layer.w = (target_Q_list[mm].output_layer.w * POLYAK_CONSTANT) + (Q_model_list[mm].output_layer.w * (1.0-POLYAK_CONSTANT))
    target_Q_list[mm].output_layer.b = (target_Q_list[mm].output_layer.b * POLYAK_CONSTANT) + (Q_model_list[mm].output_layer.b * (1.0-POLYAK_CONSTANT))
  return


def test_networks(Q_model_list, target_Q_list, policy_model, replay_dataset):
  """
  Testing each network once to ensure they are working correctly
  """
  replay_dataset = replay_dataset.batch(1)
  repeat=0
  for minibatch in replay_dataset:
    s, a, r, n_s, d = minibatch

    #Testing Target Functions
    actions_tensor, _ = sample_action_with_entropy(policy_model, n_s)
    input = concatenate_state_action_Q(n_s, actions_tensor)
    tQ_out_0 = target_Q_list[0](input)
    tQ_out_1 = target_Q_list[1](input)
    TD_target = tf.math.minimum(tQ_out_0, tQ_out_1)
    print("TD_target without reward, if it is extremely low value as if untrained, something is wrong: ", TD_target)
    TD_target += r

    #Testing Q Functions
    out_0 = Q_model_list[0](concatenate_state_action_Q(s, a))
    out_1 = Q_model_list[1](concatenate_state_action_Q(s, a))
    print("Q_0 loss: ", TD_target - out_0, " Q_1 loss: ", TD_target - out_1)
    
    #Testing Policy, print the loss on the output vector, std is the second part
    grads, _ = train_policy(policy_model, Q_model_list, minibatch, 1)
    print("Policy output grads (1st is mean, 2nd is std): ", grads[len(grads) - 1])

    repeat+=1
    if repeat>=2:
      return
  #-------------------------------------------------------------------------------------------------------------#
