#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Methods to run through RL environments using SAC
replay_buffer, sample_action, concatenate_state_action_Q, action_chance

@author: Joe Doerr
"""

import sys

from tensorflow._api.v2 import random
sys.path.append("../")
import tensorflow as tf
import gym
import time
import os
import numpy as np
from action_normalizer import ActionNormalizer
currentdir = os.path.dirname(os.path.realpath(__file__)) # dir to folder holding this file

#includes from own files:
from SAC.run import replay_buffer, sample_action, save_all_networks, set_all_networks, sample_deterministic_action
from SAC.train import training_instance_anybs, train_target_models, test_networks
from tflog import Make_summary_info
from SAC.hyperparameters_SAC import *

log_path=currentdir+'/'+'logs'
writer = tf.summary.create_file_writer(log_path)

# setup correct action clipping, learning each update with large batch size, correct polyak averaging updates

def main():
    start_time = time.time()
    randomly_choose_steps = RANDOMLY_CHOOSE_STEPS
    num_timesteps = TIMESTEPS
    num_epochs = EPOCHS
    num_batches = NUM_BATCHES
    batch_size = BATCH_SIZE
    step=0
    print("number of timesteps of data: ", num_epochs * num_timesteps, " number of timestep updates: ", (num_epochs - randomly_choose_steps) * num_batches * batch_size)
    print("number of data saved until target averages into value of state for TD error (see if it is still in replay):", (1.0 / (1-POLYAK_CONSTANT)) * (num_timesteps))

    target_Q_list, Q_list, Policy = set_all_networks(NETWORK_LAYERS, NETWORK_LAYER_SIZE, 3, 1) #was 256 when sorta seeming promisingS, 128 seemed promising as well
    #Policy.load_weights(currentdir + '/saved_models/model_set_%s' %1 + '/Policy')
    #Q_list[0].load_weights(currentdir + '/saved_models/model_set_%s' %1 + '/Q_1')
    #Q_list[1].load_weights(currentdir + '/saved_models/model_set_%s' %1 + '/Q_2')
    #target_Q_list[0].load_weights(currentdir + '/saved_models/model_set_%s' %1 + '/tQ_1')
    #target_Q_list[1].load_weights(currentdir + '/saved_models/model_set_%s' %1 + '/tQ_2')

    env = gym.make('Pendulum-v1')
    env = ActionNormalizer(env)
    s = env.reset()
    a = env.action_space.sample()
    s_prime, r, d, _ = env.step(a)
    replay = replay_buffer(100000, [s, a, r, s_prime, d])
    print(replay.data)

    #repeat
    for epochs in range(num_epochs): 
        print("epoch: ", epochs)
        av_reward = 0.0

        if epochs % RENDER_EPOCH == 0 and epochs > randomly_choose_steps+NUM_STEPS_PER_UPDATE_SET: #running render
            observation = env.reset()
            for timesteps in range(num_timesteps-10):
                action = sample_action(Policy, replay.list_to_data(observation))
                observation, _, _, _ = env.step(action)
                env.render()

        observation = env.reset()
        for timesteps in range(num_timesteps+1):

            if epochs < randomly_choose_steps:
                action = env.action_space.sample()
            else:
                action = sample_action(Policy, replay.list_to_data(observation)) #replay.list_to_data converts list in 2 ndim tensor

                if timesteps % NUM_STEPS_PER_UPDATE_SET == 0 and timesteps != 0:
                    step = training_instance_anybs(Q_list, target_Q_list, Policy, replay.make_dataset(), num_batches, batch_size, step, 1)
                    train_target_models(Q_list, target_Q_list)

            state = observation
            observation, reward, done, _ = env.step(action)

            replay.add_data([state, action, reward, observation, done])
            av_reward += float(reward) #tensorboard logging

            if done == True:
                print("Episode finished after {} timesteps".format(timesteps+1))
                break

        if epochs > randomly_choose_steps:
            Make_summary_info({'av_reward':(av_reward/float(num_timesteps))}, writer, epochs) #tensorboard logging
            print("epoch done: ", epochs, " av_reward: ", av_reward/float(num_timesteps))
            if epochs % TEST_NETWORKS == 0:
                test_networks(Q_list, target_Q_list, Policy, replay.make_dataset())

    print("training complete in: ", (time.time() - start_time), " seconds")

    save_all_networks(Q_list, target_Q_list, Policy, 99)
    env.close()

def use_saved_policy():
    env = gym.make('Pendulum-v1')
    env = ActionNormalizer(env)
    s = env.reset()
    a = env.action_space.sample()
    s_prime, r, d, _ = env.step(a)
    replay = replay_buffer(1000, [s, a, r, s_prime, d])

    #collecting Policy weights
    _, _, Policy = set_all_networks(NETWORK_LAYERS, NETWORK_LAYER_SIZE, 3, 1) #was 256 when sorta seeming promisingS, 128 seemed promising as well
    Policy.load_weights(currentdir + '/saved_models/model_set_%s' %99 + '/Policy')

    for ii in range(100):
        observation = env.reset()
        for timesteps in range(TIMESTEPS+1):
            env.render()
            action = sample_deterministic_action(Policy, replay.list_to_data(observation))
            observation, _, done, _ = env.step(action)
            print(timesteps)
            if done == True:
                print("Episode finished after {} timesteps".format(timesteps+1))
                break

    env.close()

if __name__ == "__main__":
    main()
    #use_saved_policy()
