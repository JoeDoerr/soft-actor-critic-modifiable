#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Changeable hyperparameters for train.py and run.py
SCALE_E_INNER and SCALE_E_OUTER:
scaler = e^(SCALE_E_INNER * loss * -1) * SCALE_E_OUTER
SCALE_E_INNER=0.3
SCALE_E_OUTER=0.5
This will cause loss of -2.3 to reduce the entropy, past there exponentially get worse, with at loss -10 multiplying entropy by 10
This scaler scales the entropy term in the policy update
The idea is that poorly performing actions should have a boost in entropy further than SAC natural boosts

@author: Joe Doerr
"""

LOGSTD_MIN = -20.0
LOGSTD_MAX = 2.0

IND_NOISE_STD = 1.0

ALPHA = 0.3
GAMMA = 0.99
POLYAK_CONSTANT = 0.995
LEARNING_RATE = 0.0001

"""
Changeable hyperparameters for whichever RL file
"""
EPOCHS = 300
RANDOMLY_CHOOSE_STEPS = 75
TIMESTEPS = 200
NUM_STEPS_PER_UPDATE_SET = 1
#START_UPDATING = 100
NUM_BATCHES = 1
BATCH_SIZE = 72

NETWORK_LAYERS = 2
NETWORK_LAYER_SIZE = 256

RENDER_EPOCH = 3
TEST_NETWORKS = 10
