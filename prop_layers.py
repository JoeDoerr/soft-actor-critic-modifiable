#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Propagation Layers and Model using Keras Subclassing

@author: Joe Doerr
"""

import sys
sys.path.append("../")

import numpy as np
import tensorflow as tf
from tensorflow import keras

#use keras subclassing and make a layer class with a weight variable matrix
#then a variable vector for bias
#then have the call function take in the vector of input, matrix multiply it to the weight
#then take that output and elementwise sum with the bias for the layer output
#make the model class and when building it choose how many of the layer classes it wants
#then have that call function just call each layer in order
#use gradient tape for updating to find the gradient then apply it
#things like dropout and batchnorm would have to be manually made
import time
class dense_layer(keras.layers.Layer):
  def __init__(self, prev_n, n, relu=True, tanh=False, softmax=False, name="default", batch_norm=True):
    super(dense_layer, self).__init__()

    w_init = tf.random_uniform_initializer()
    #input is 784, so [784] * [784][512] means that it will output a [512]
    self.w = tf.Variable(initial_value=w_init(shape=(prev_n, n), dtype=tf.float64), trainable=True, name=name+"_weight")

    self.b = tf.Variable(initial_value=w_init(shape=(n,), dtype=tf.float64), trainable=True, name=name+"_bias")

    self.relu = relu
    self.softmax = softmax
    self.tanh = tanh

  def call(self, input):
    input = tf.cast(input, dtype=tf.float64)
    #print(input.shape)
    #print(self.w.shape)
    x = tf.matmul(input, self.w)
    #if tf.reduce_any(tf.math.is_nan(x)) == True:
      #print(input)
      #print("dense layer propagation nan")
      #print(self.w.name)
      #print(x)
      #time.sleep(1)

    x = x + self.b
    if self.relu == True:
      x = tf.nn.relu(x)
    elif self.softmax == True:
      x = tf.map_fn(tf.nn.softmax, x)
      for each in x:
        tf.nn.softmax(each)
    elif self.tanh == True:
      x = tf.nn.tanh(x)
    #print(x)
    #if self.batch_norm == True:
      #---
    return x

class nn_model(keras.Model):
  def __init__(self, numLayers, input_size, layer_size, output_size, relu = True, tanh = False, softmax_out = False, tanh_out = True, name="default"):
    super(nn_model, self).__init__()

    #make input, middle, and output layers
    self.dense_layers = []

    self.dense_layers.append(dense_layer(input_size, layer_size, relu=relu, tanh=tanh, name=name))

    for ii in range(numLayers-1):
      self.dense_layers.append(dense_layer(layer_size, layer_size, relu=relu, tanh=tanh, name=name))

    self.output_layer = (dense_layer(layer_size, output_size, relu=False, tanh=tanh_out, softmax=softmax_out, name=name))
  
  def call(self, input):
    x = input
    for ii in range(len(self.dense_layers)):
      x = self.dense_layers[ii](x)
    return self.output_layer(x)