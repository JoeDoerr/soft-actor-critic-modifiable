#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Logging utilities

@author: Joe Doerr with Zheyuan Zhu
"""
import tensorflow as tf
def Make_summary_info(scalar_dict, writer, step_num): #for mask_dict, input lm.masks[num].w
  with writer.as_default():

    for key in scalar_dict:
      tf.summary.scalar(key,scalar_dict[key],step=step_num)

  writer.flush()
###%%%###

def Make_summary_info_image(image_dict, writer, step_num): #for mask_dict, input lm.masks[num].w
  with writer.as_default():

    for key in image_dict:
      tf.summary.image(key,image_dict[key],step=step_num)

  writer.flush()