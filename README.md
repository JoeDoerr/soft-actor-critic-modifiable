# Soft Actor Critic (SAC) and Deep Deterministic Policy Gradient (DDPG) algorithms coded in python using tensorflow. #
------------
# Goals #
------------
### Create an easily modifiable and changeable soft actor critic and DDPG algorithm for the ability to quickly try variations.

### Personally handcode SAC and DDPG to gather a deeper understanding of the algorithms.

### Share what I went through to accomplish this to hopefully help others working on the same type of project.

### Give an easy to understand but in-depth explanation of the functionality of each portion of these algorithms.

# Conceptual understandings I feel are valuable #
------------
### Understanding the concept of continuous action spaces: 
Continuous action spaces have the policy network output a vector that is used as the action. A deterministic actor will use the vector raw. A stochastic actor will change the vector in some way as exploration. 
In DDPG there is OUnoise added to the vector. In SAC the vector uses its own standard deviation output to scale normal distribution (mean=0, std=1) noise to apply to each element of the action vector.

### How are the networks set up for SAC?: 
There is a policy network, 2 Q networks, and 2 targ_Q networks. For me, there are no activations functions on the outputs of these networks.
The hidden layer activations are ReLU. The policy network outputs a vector that is sized to what the action needs to be, and a same-sized corresponding standard deviation vector. 
I have seen people use a few separate hidden layers for the std and action output but I just have it as an (action_space * 2) sized output where I choose half as action and half as std. 
The outputs of the standard deviation vector are clipped between -20 and 2 and are used as std=e^raw_output when used.

### Understanding reparameterization trick: 
The reparameterization trick is so that we can chain rule the Q(s,a) residual at the inputted action directly to the Policy. This means we can direction update to the action that 
Q(s,a) considers the best. 
One thing they mention about the reparameterization trick is that they want to make the objective function over an expectation of noise rather than expectation of actions. 
When the update is over the expectation of actions, that means that to get the full and correct update, each action will have to be run. 
We see this in Vanilla Policy Gradient's REINFORCE and actor critic where the gradient is E{Reward or Q(s,a) * GRADIENT(ln(prob(a|s)))} 
This means that to find the best action to take in state s, each action has to be used and the better actions will have a higher scaled gradient than others. 
If you don't pass in every action, its very likely that the best one is going to be not there to get updated towards. 
The reparameterization trick allows the Q(s,a) network to directly tell the Policy what the output should have been to get a higher reward. 
The rate of change of Q with respect to the inputted action will increase Q when added to the action value, and thats exactly what happens. 

As a side note, can think of this like the rate of change of Q with respect to the inputted action if positive means that increasing the inputted action increases Q. When negative, increasing the inputted 
action decreases Q. Therefore, when adding the negative derivative to the inputted action, it makes the inputted action go in the other direction (decrease), which will increase the Q. 

### Where do the mean and std of the Policy learn to move towards?: 
Given the reparameterization trick there is an expectation over noise. Expectation over noise means that the algorithm wants the best mean and std for the Policy given all noise. 
The mean moves with consideration of its std. Over lots of sampled noise and given the std, where does the mean sit to get the best rewards? 
This means that the mean does not move towards the best deterministic action, but rather where, given its std and noise, does it get the best rewards. 
I like to think of the std as lines pushing out of the mean where it wants the best rewards in its whole wingspan: ------mean------  

The std moves with consideration of its mean. Over lots of sampled noise and given the mean, how much should the wingspan adjust to get the best rewards? 
If more of the time (expectation over noise) the std scaling the noise so high or so low is good for rewards or bad for rewards given by the Q(s,a) chain rule backpropagation, 
the std will increase or decrease.

The mean and the std changing is really the fundamental part of the SAC algorithm. We want to choose the best actions while acting the most randomly as possible. 
With the entropy term constantly pushing to increase std, each state-action always pushes to exactly how far it can spread its allowed to increase its std and still do well.

At the end of training, SAC uses no noise and only the mean to make decisions. This works very well. So the mean eventually does go towards the correct action? 
The answer to this is yes. The beauty of the SAC algorithm is that when the mean sits on the action that Q thinks is the best, the std collapses very quickly 
as every single noise sample will always tell the std to reduce so it can be closer to the mean. Intuitively this throughout training would be bad. 
I ran a test where I pushed the mean towards the correct action and found that this broke the algorithm where the std collapsed instantly and there was no exploration. 
The mean eventually reaches the correct action by the infinite horizon TD rewards of the best action becoming significantly higher than the other rewards. 
Recall that the Q function includes an entropy term to overvalue actions that are randomly chosen. Given enough time, the correct action will have much 
higher rewards than the random actions around it that are boosted by entropy. We then need two things to happen for the mean to move to the correct action. 
The std needs to want to decrease then given the std decreased the mean with a smaller wingspan will be coerced to move towards the actual correct action: --mean-- 
The std gets smaller when the correct action beats out the higher entropy actions by enough. This is where tuning entropy can be pretty important. 
When the correct action gets strong enough, the std over the expectation of noise will decrease as going to the correct action is better enough than the other actions. 
Then while this happens, the mean will have a smaller wingspan and will have less distractions from moving towards the correct action as over the expectation of 
noise it will be very close to just being the mean after the noise is applied. This is a rather important distinction about the importance of tuning the entropy 
parameter and a more in-depth reason for the entropy term in the Q-function evaluations.

### Why are target functions and soft updates (polyak averaging) so important for stability while training?: 
Infnite horizon MDPs have TD-targets that can be huge. Their values are limited by lambda, with a lambda of 0.9 lowering rewards past 20 steps away to very low. 
SAC and DDPG both use target functions and soft updates. Target functions affect the calculation of TD-targets and therefore has a strong effect on the whole algorithm. 
You may have read that target functions greatly increase stability. Here is why: 
When any state-action pair(1) is updated where its value changes to a potent amount, any state-action(2) that uses that given state-action in its TD-target will also change to a potent amount. 
The same phenomemon goes for this state-action(2) pair that just updated with this state-action pair(1) as its TD-target. If there are generally high rewards, just because we updated (2),
(2) will have a massively overvalued reward compared to other state-action pairs. So if we run even a batch of 100 to get some initial changes then another batch of 100, every state-action 
that was randomly sampled and had a TD-target of one of the value from the first batch will now be extremely overvalued. Not to mention how far this will snowball if we sampled based on this 
and continue to run 100 batch updates. 

The way to combat this is with target functions and soft updates of those target functions. 
Consider the polyak constant or what others consider tau which is tau=(1-polyak). I like to think of it has 1/(1-polyak) is the number of updates before the target networks reach what the 
network currently is. And lets say the batch size is 100. That means that there are about 100 * (1/(1-polyak)) samples learned before the updates are used in TD-targets. 
They are soft updates so the updates will slowly be used more and more in TD-targets but this is one way to think about it.

Think of it as a level playing field. All samples in the replay data learn off of what is currently available as TD targets. Once they all learn, it goes to the next step by the target 
functions averaging towards the current functions where all the 
current rewards become available in the TD-targets. This avoids the aforementioned problem where anything simply updated at all would get super overvalued. With this method of target 
functions and polyak averaging, the state-action evaluations all wait for each other to learn their values before moving on to using those learned values as future rewards. 
This also ensures that we get the correct reward for each state-action before using them as future rewards for other state-actions. Giving state-actions incorrect future rewards 
is much harder to deal with than the state-action itself having incorrect rewards that it can fix rather simply by just updating itself a few times. Incorrect future rewards to other state-actions 
means updating every state-action that used those incorrect future rewards. 

### How does SAC learn complex behaviors? 
One thing that I was very impressed about in the Pendulum-v1 solution for SAC was how it would learn to swing back and forth to gain momentum to get to the top. The way this happens is through 
the TD-rewards and the soft updates of the target functions. Given any good eventual state such as reaching high up on the pendulum, how does this delayed reward teach a facing down pendulum to 
get momentum then swing up to the top? This is my understanding: We can consider being on the upper side of the pendulum with the momentum to go to the very top and stay there as good rewards. 
From those state-action pairs, any state that chains into them will slowly gain their rewards from the soft updates of the target functions making their rewards slowly available. For each state in the chain to take 
part in this high eventual reward, they need to sample the good action that gets them that next state-action TD reward. This is done through pure exploration luck. In the pendulum it is rather likely to try 
an action that is close enough so it is essentially guaranteed for the exploration to be sufficient. Given the state in the chain that can reach the next state, there needs to be (s,a,s') replay memory 
where the s' is the state where we can get the high reward. For SAC the policy hopefully will eventually over multiple runs of this (s,a,s') datapoint run the correct a' to get the reward. It is more likely 
to than not. For DDPG a reason that it is faster than SAC for the pendulum is that it will run a' instantly as it is deterministic in TD-rewards and has learned that a' is the best to take at s'. The state s 
needs to take action a to get to state s' and for SAC when we sample this we need to take the correct a' to get the high reward. Once this happens, this state s will now learn that action a is good and will likely 
take it more in the future so we can continue to train off of this (s,a) to continue to update its TD-reward. Then after a bit of soft updating into target functions, the (s,a) will now have a higher reward that is 
related to this reward later down the chain. Now the next (s,a) can learn using the before mentioned (s,a) as its TD-reward and take part in this delayed future reward as well. From here it just has to do the 
same process for each action in the chain towards getting this delayed reward. That means that eventually the pendulum sitting at the bottom will learn to swing itself out and go to the top by knowing that each 
state it reaches by taking each action has a known connection to an eventual high reward. This is a difficult concept to grasp and I hope I am correct in my intuitions about the process. 

# Properties of SAC #
------------
* SAC has two Q functions and two target Q functions and a policy. These are all neural networks.
* SAC runs epochs for some amount of time then updates all networks. This repeats until convergence.
* SAC is an off policy algorithm so it gathers (s,a,r,s',d) from each step as data into a replay buffer to randomly batch sample during training. (d is 0 if not terminal, 1 is terminal)
* The two Q functions get updated using MSBE (Mean Squared Bellman Error) where the truth value of the Q(s,a) is the decayed lower of the two target Q functions + r, a~current policy
* The policy gets updated with only the state of the data, choosing its own action. It maximizes the expected value of [Q(s,a) + entropy term] over the action distribution ~ policy. It uses a reparameterization trick to have the actions used deterministically sampled from the policy, with the expected value being over gaussian noise used in the reparameterization trick and state distribution. This reparameterization trick allows the policy parameters to chain rule off of the Q(s,a) and entropy term for effective updating.
* Using Polyak averaging with some Polyak constant ~ 0.995, the target Q functions get their parameters averaged into their respective Q functions, targQ x Polyak + Q x (1-Polyak)

# Issues I encountered along the way #
------------
SAC:

* Problems with the standard deviation (std):
I was using the raw std output of the Policy network as the std. This meant I had -std sometimes which made no sense. Decreasing a negative std essentially increases the std.
I started reading other people's repositories and found that the std should be calculated as std=e^policy_network_std_output. 
This meant that the gradients could correctly move the policy lower and higher as e^x is always positive and is higher if the x is higher and lower if the x is lower regardless of positive or negative values.

* Nan and inf issues with log became problematic as log(0)=inf so I ended up creating a function to run log but adds 1e-6 if 0

* Problems with probability density calculation: 
The probability density function is (1/sqrt(2pi * std^2)) * e^((sample-mean)/std^2). I was mislead by some sources and would use incorrect ones. When reading papers or just information on RL, 
in continuous action spaces, Policy(a|s) 
MEANS THE PROBABILITY DENSITY OF ACTION a GIVEN STATE s. In discrete action spaces it will generally be the outputted chance to take that action. 
Probability density is different from probability and can be >1. (Reminder to use std=e^policy_network_std_output


* Problems with entropy calculation: 
I was very invested in being able to see the rate of change of the std with respect to entropy be positive at all points on desmos to ensure that I was using the correct formula. 
When including the reparameterization trick's calculation, it is important to note that if you set the means to be different (they will never be different), the derivative gets ruined with 
part of the time the rate of change of the std with respect to entropy being negative. You can try this for yourself in the desmos graphing calculator link below. 
The intuition is that since e^((sample-mean)/std^2) had std in the denominator that increasing the std would reduce the overall value which increases -log() value. 
(1/sqrt(2pi * std^2)) also has std increasing causing an increase in the -log() value. That means the entropy term in the Policy update pushes to increase std.
Here is the link to my correct formulation of the entropy: https://www.desmos.com/calculator/z02bvv4w2q

* @tf.function on the TD_reward_calculation somehow didn't use any updated versions of my target functions. This meant that future rewards were not considered at all and my SAC and DDPG algorithms did 
not work at all. This was actually incredibly difficult to realize and I spent a few days very frusturated. I realized something was wrong when I saw Q function loss graph over time just being simply low 
and never spiking like other graphs would because there was never new targets. To now avoid any sort of problem like this I have a function test_networks in train.py for if the algorithm stops working 
I can check what exactly is going wrong. I also have tensorboard visualization ready to go so I can watch the graphs as well.

* Update way more than you sample. For DDPG at least doing a batch of 100 and 1 target function update for every single timestep was helpful. Also gather lots of random step data at the start before training.


# File Functions #
------------
```
SAC File Contents

train:
[training_instance_anybs]: 
	Works for any batch size to fit in a gpu, run this to run the whole optimization routine
[get_TD_target]: 
	Uses clipped double-Q trick to get the TD target to update the 2 Q functions with
[train_real_Qs]: 
	Calculates the gradients for the 2 Q functions
[train_policy]: 
	Calculates the gradients for the policy_model
[train_target_models]: 
	Polyak averaging for the 2 target Q functions
[test_networks]:
	checks if each network is running correctly, easy to find errors if something goes wrong or what network gets poorly affected by some change I made

run:
[class replay_buffer]: 
	Makes a replay buffer class instance to hold all the data (s,a,r,s',d)
[replay_buffer]->
	list_to_data: helps for when data is not in tensor form, changes list to tensor that can be used in dataset
[replay_buffer]->
	add_data: Adds data to the replay buffer and if its full removes the oldest piece of data
[replay_buffer]->
	make_dataset: sets up a dataset to use in training
[sample_action]: 
	Samples action from the policy output given the continuous action vector tensor and the standard deviation tensor, using zero-mean gaussian noise
[sample_deterministic_action]: 
	Samples the action mean, not necessarily useful as sample_action with noise_std=0 will accomplish the same thing
[concatenate_state_action_Q]: 
	State and action standardized concatenation for input into the Q functions
[log_avoid_inf_nan]: 
	performs log and if inf or nan adds 1e-8 to ensure the inputs to log aren't 0
[sample_prob_density]: 
	calculates the sample's probability density with (1/sqrt(2pi * std^2)) * e^((sample-mean)/std^2), this is equivalent to Policy(a|s)
[sample_pre_tanh_action]: 
	samples action before applying tanh
[entropy_of_pre_tanh_action]: 
	using sample_prob_density(), calculates the entropy of a pre_tanh_action (never use tanh_action in the prob density)
[sample_action_with_entropy]: 
	finds entropy of pre_tanh_action then outputs the entropy and tanh action.
Entropy calculated with tanh as: sum over action vector-> -log(probdensity(pre_tanh_action)) + log(1-tanh(pre_tanh_action)^2)
Note if want to scale actions can scale output action by scalar, and for this portion of entropy calculate as: log(scalar * (1-tanh(pre_tanh_action)^2)), and to see the mean it is: scalar * tanh(mean)
```

# Relation of the Policy Gradient Theorem in PG, to SAC reparameterization policy gradient updating found in SAC (with mention of DDPG policy gradient calculation) #
------------
In the Policy Gradient Theorem used to formulate the original PG gradient, the expected value over a distribution of actions is equivalent to the 
summation over each action in the action space where the inside is the chance that the action is chosen given the state, policy(a|s), times the Q(s,a) value. 
This formulation can be derived with respect to the policy parameters. Through the product rule, Q(s,a) needs to be derived with respect to policy parameters, 
{deriving Q(s,a) with respect to policy parameters is actually removed from DDPG where they consider Q(s,a) immutable}. To maximize Q(s,a) with respect to policy parameters, 
maximizing the value function which makes up Q(s,a) is how to do it. This has the final gradient of the objective function result in the expected value over the state distribution 
and action distribution[gradient(policy(a|s)) * Q(s,a)]. Each iteration update of PG works towards this expected value by doing one state-action update at a time. The math is correct 
so theorhetically if each action in the action space was updated on, the action with the best Q(s,a) value will have the gradient lean the most towards increasing it over all other 
actions. This is the same that happens in the reparameterization in SAC to maximize the expected value of the Q function over the state and action distribution. However, in the SAC 
reparameterization, the expected value is moved towards much faster. By considering the policy as an input to the Q function, it can be chain ruled to and the way to change the policy 
parameters to improve the Q function can directly be applied each step. This causes each SAC gradient to be more improving of the action with the best Q(s,a) rather than each PG gradient 
working with one action at a time, needing to do all of them to correctly update the best action in comparison to other worse actions.

Policy Gradient can also be derived as Q(s,a) not being a function of the policy which brings the Q(s,a) to evaluate a given s,a to a certain value. That makes the derivation simple, 
with summation over actions and gradient of policy(a|s) * Reward(s,a). Log derivative trick of p(s|a) * gra(p(s|a)) / p(s|a) = gra(p(s|a)) = p(s|a) * gra(log(p(s|a))), so our earlier 
equation comes to: expected value over actions sampled from policy [Reward(s,a) * gra(log(p(s|a)))]. This is equivalent to the earlier defined PG derivation and suffers from the same 
problem, and that problem SAC fixes.

# Sources #
------------
Tests inspired by https://arxiv.org/pdf/1801.01290.pdf
(Haarnoja et al., 2018)

helpful resources:

https://spinningup.openai.com/en/latest/algorithms/sac.html

https://lilianweng.github.io/lil-log/2018/04/08/policy-gradient-algorithms.html

https://towardsdatascience.com/deep-deterministic-policy-gradient-ddpg-theory-and-implementation-747a3010e82f