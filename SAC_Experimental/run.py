#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Methods to run through RL environments using SAC
replay_buffer, sample_action, concatenate_state_action_Q, action_chance

@author: Joe Doerr
"""

import sys
from threading import active_count
sys.path.append("../")
import tensorflow as tf
from prop_layers import nn_model
from SAC_Experimental.hyperparameters import *

#replay buffer class, function to add new data and release old data, shuffle by full size
#keep unshuffled dataset that we keep manipulating then always pass in a newly shuffled copy
class replay_buffer():
    def __init__(self, size, new_data_list):
        self.size = size
        #data kept in tensors, we make these variables so we can more easily specifically access datapoints
        self.state = self.list_to_data(new_data_list[0])
        self.action = self.list_to_data(new_data_list[1])
        self.reward = self.list_to_data(new_data_list[2])
        self.n_state = self.list_to_data(new_data_list[3])
        self.d = self.list_to_data(float(new_data_list[4]))
        self.data = [self.state, self.action, self.reward, self.n_state, self.d]

    def list_to_data(self, input):
        tensor = tf.expand_dims(tf.squeeze(tf.convert_to_tensor(input)), 0) #make the batch dimension on axis=0
        if len(tensor.shape) == 1:
            tensor = tf.expand_dims(tensor, 1)
        return tf.cast(tensor, dtype=tf.float64)

    def add_data(self, new_data_list): #state, action, reward, n_state, d
        #new_data_list[4] = float(new_data_list[4]) #ensuring that d is an float
        for ii in range(len(self.data)):
            self.data[ii] = tf.concat([self.data[ii], self.list_to_data(new_data_list[ii])], 0) #concat along batch dimension
            if self.data[ii].shape[0] >= self.size:
                self.data[ii] = self.data[ii][1:, :] #taking off the oldest piece of data
    
    def make_dataset(self):
        #make with tensorslices then shuffle then return
        train_dataset=tf.data.Dataset.from_tensor_slices((self.data[0], self.data[1], self.data[2], self.data[3], self.data[4])).shuffle(self.size)
        return train_dataset

def concatenate_state_action_Q(states_tensor, actions_tensor):
    # shape of states_tensor and actions_tensor [batch size, tensor of given datapoint]
    sa = tf.concat([tf.cast(states_tensor, dtype=tf.float64), tf.cast(actions_tensor, dtype=tf.float64)], 1) #axis = 1 as we aren't adding the tensors to the same batch size axis=0, but putting the tensors together at each batch
    return sa

#for quick environment action sampling
def sample_action(policy_model, states_tensor, noise_std=IND_NOISE_STD):
    combined_tensor = policy_model(states_tensor) #outputs a [batch size, output vector size]
    mean, log_std = tf.split(combined_tensor, 2, axis=1)
    log_std = tf.clip_by_value(log_std, LOGSTD_MIN, LOGSTD_MAX)
    std = tf.exp(log_std) #e^log_std

    o = tf.random.normal(std.shape, 0, noise_std, dtype=tf.float64)
    action = mean + (o * std)

    #action = tf.math.tanh(active_count)
    action = tf.math.tanh(action)

    if tf.math.is_nan(action[0, 0]) == True:
        print("sample action is nan")

    return action

def sample_deterministic_action(policy_model, states_tensor):
    combined_tensor = policy_model(states_tensor) #outputs a [batch size, output vector size]
    action, _ = tf.split(combined_tensor, 2, axis=1)
    return action

def log_avoid_inf_nan(input): #simply does log(input)
  if tf.reduce_any(tf.math.is_nan(tf.math.log(input))) == True or tf.reduce_any(tf.math.is_inf(tf.math.log(input))) == True:
    print("nan or inf from log, however we fixed it")
    return tf.math.log(input + 0.00000001)
  else:
    return tf.math.log(input) #NEGATIVES AND 0 MAKES LOG NAN

import math as m
def sample_prob_density(pre_tanh_actions_tensor, mean, std): #probability density of sample pre_tanh_actions_tensor
    #rate of change of std with respect to increasing -log(sample_prob_density) is positive always, a straight line
    a_mu_diff = tf.square(pre_tanh_actions_tensor - mean) #actions tensor is (mu + (e^log_std * sampled noise))
    a_into_e = a_mu_diff / tf.square(std)
    apd = tf.exp(-1 * a_into_e)
    apd /= tf.pow(2 * m.pi * tf.square(std), 0.5)
    return apd

def action_bound_apd_term(pre_tanh_actions_tensor): #apd is action probability density, this is to account for tanh squashing
    action = tf.square(tf.tanh(pre_tanh_actions_tensor))
    return 1 - action

def sample_pre_tanh_action(policy_model, states_tensor):
    combined_tensor = policy_model(states_tensor) #outputs a [batch size, output vector size]
    mean, log_std = tf.split(combined_tensor, 2, axis=1)
    log_std = tf.clip_by_value(log_std, LOGSTD_MIN, LOGSTD_MAX)
    std = tf.exp(log_std) #e^log_std
    o = tf.random.normal(std.shape, 0, IND_NOISE_STD, dtype=tf.float64)
    pre_tanh_action = mean + (o * std)
    return pre_tanh_action, mean, std

def entropy_of_pre_tanh_action(pre_tanh_action, mean, std):
    entropy = (-1 * log_avoid_inf_nan(sample_prob_density(pre_tanh_action, mean, std)))+\
                log_avoid_inf_nan(action_bound_apd_term(pre_tanh_action))
    entropy = tf.reduce_sum(entropy, axis=1, keepdims=True)
    return entropy

#for training to get probability density of action
def sample_action_with_entropy(policy_model, states_tensor):
    combined_tensor = policy_model(states_tensor) #outputs a [batch size, output vector size]
    mean, log_std = tf.split(combined_tensor, 2, axis=1)
    log_std = tf.clip_by_value(log_std, LOGSTD_MIN, LOGSTD_MAX)
    std = tf.exp(log_std) #e^log_std

    o = tf.random.normal(std.shape, 0, IND_NOISE_STD, dtype=tf.float64)
    pre_tanh_action = mean + (o * std)

    #getting the tensor version of entropy, see appendix of SAC Haarnoja et al., 2018 for this calculation
    entropy = (-1 * log_avoid_inf_nan(sample_prob_density(pre_tanh_action, mean, std)))+\
                log_avoid_inf_nan(action_bound_apd_term(pre_tanh_action))

    #print("sample probability density:, ", sample_prob_density(pre_tanh_action, mean, std))
    #print("entropy:, ", -1 * log_avoid_inf_nan(sample_prob_density(pre_tanh_action, mean, std)))
    #print("tanh entropy part:, ", log_avoid_inf_nan(action_bound_apd_term(pre_tanh_action)))
    #getting a scalar probability
    entropy = tf.reduce_sum(entropy, axis=1, keepdims=True) #run the summation on not the batch dimension, for each batch sum the action vector

    #action = tf.math.tanh(active_count)
    action = tf.math.tanh(pre_tanh_action) #getting the action to output

    return action, entropy


def set_all_networks(layer_num_all, layer_size_all, state_size, action_size): #no output activation function for any
    #(self, numLayers, input_size, layer_size, output_size, relu = True, tanh = False, softmax_out = False, tanh_out = True):
    Q_targ_1 = nn_model(layer_num_all, (state_size + action_size), layer_size_all, 1, relu=True, tanh=False, softmax_out=False, tanh_out=False, name="Q_targ_1")
    Q_targ_2 = nn_model(layer_num_all, (state_size + action_size), layer_size_all, 1, relu=True, tanh=False, softmax_out=False, tanh_out=False, name="Q_targ_2")
    Q_1 = nn_model(layer_num_all, (state_size + action_size), layer_size_all, 1, relu=True, tanh=False, softmax_out=False, tanh_out=False, name="Q_1")
    Q_2 = nn_model(layer_num_all, (state_size + action_size), layer_size_all, 1, relu=True, tanh=False, softmax_out=False, tanh_out=False, name="Q_2")
    Policy = nn_model(layer_num_all, state_size, layer_size_all, (action_size * 2), relu=True, tanh=False, softmax_out=False, tanh_out=False, name="Policy")
    Q_targ_list = [Q_targ_1, Q_targ_2]
    Q_list = [Q_1, Q_2]
    return Q_targ_list, Q_list, Policy

import os
def save_all_networks(Q_list, tQ_list, Policy, model_save_set=0):
    currentdir = os.path.dirname(os.path.realpath(__file__)) # dir to folder holding this file
    dir = currentdir + '/saved_models/model_set_%s' %model_save_set + '/'
    dir_Q_1 = dir + 'Q_1'
    dir_Q_2 = dir + 'Q_2'
    dir_tQ_1 = dir + 'tQ_1'
    dir_tQ_2 = dir + 'tQ_2'
    dir_Policy = dir + 'Policy'
    Q_list[0].save_weights(dir_Q_1)
    Q_list[1].save_weights(dir_Q_2)
    tQ_list[0].save_weights(dir_tQ_1)
    tQ_list[1].save_weights(dir_tQ_2)
    Policy.save_weights(dir_Policy)




def depreciated_action_chance(actions_tensor, states_tensor, policy_model, constant=2.5): #sqrt(2pi) ~~ 2.5
    #go through (1/constant) * e^-1((a-mean)^2 / std^2)
    #print("action chance:")
    combined_tensor = policy_model(states_tensor) #outputs a [batch size, output vector size]
    mean, std = tf.split(combined_tensor, 2, axis=1)
    std = tf.math.abs(std)
    action_chance = actions_tensor - mean
    #print(action_chance) #real
    #how high the square of std is compared to the difference makes the lowering of e less, making e higher
    action_chance /= std
    action_chance = tf.math.square(action_chance)
    #print("value to put into exp")
    #print(action_chance) #real
    action_chance = tf.math.exp(action_chance * -0.5)
    #print(action_chance) #real low
    #the lower the std, the higher the chance of each action
    action_chance /= (constant * std)
    #print(action_chance) #real high, and negative which makes log 
    z_score = (actions_tensor-mean) / std #z-score
    #print("zscore: ", z_score)
    action_chance = (1/constant) * tf.math.exp(-0.5 * tf.math.square(z_score))
    #now each individual value in the [output vector size] of [batch size, output vector size] is the probability of that happening, II them together to get chance of action
    action_chance = tf.math.reduce_prod(action_chance, 1) #each [output vector size] reduce_prods within itself
    #print("chances of each action:")
    print(action_chance[0]) #real high
    return action_chance #we multiply by -1 so inputs to log should be < 1