#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Methods to run through RL environments using SAC
replay_buffer, sample_action, concatenate_state_action_Q, action_chance

@author: Joe Doerr
"""

import sys

from tensorflow._api.v2 import random
sys.path.append("../")
import tensorflow as tf
import gym
import time
import math
import os
import numpy as np
currentdir = os.path.dirname(os.path.realpath(__file__)) # dir to folder holding this file

#includes from own files:
from SAC_Experimental.run import replay_buffer, sample_action, save_all_networks, set_all_networks
from SAC_Experimental.train import training_instance_anybs, train_target_models, concatenate_state_action_Q, training_instance
from tflog import Make_summary_info
from SAC_Experimental.hyperparameters import *

log_path=currentdir+'/'+'logs'
writer = tf.summary.create_file_writer(log_path)


# setup correct action clipping, learning each update with large batch size, correct polyak averaging updates


class ActionNormalizer(gym.ActionWrapper):
    """Rescale and relocate the actions."""

    def action(self, action: np.ndarray) -> np.ndarray:
        """Change the range (-1, 1) to (low, high)."""
        low = self.action_space.low
        high = self.action_space.high

        scale_factor = (high - low) / 2
        reloc_factor = high - scale_factor

        action = action * scale_factor + reloc_factor
        action = np.clip(action, low, high)

        return action

    def reverse_action(self, action: np.ndarray) -> np.ndarray:
        """Change the range (low, high) to (-1, 1)."""
        low = self.action_space.low
        high = self.action_space.high

        scale_factor = (high - low) / 2
        reloc_factor = high - scale_factor

        action = (action - reloc_factor) / scale_factor
        action = np.clip(action, -1.0, 1.0)

        return action

#setup, network size is important for SAC, small network can cause it not to work
def main():
    start_time = time.time()
    randomly_choose_steps = RANDOMLY_CHOOSE_STEPS
    num_runs_per_update = NUM_RUNS_PER_UPDATE
    num_timesteps = TIMESTEPS
    num_epochs = EPOCHS
    num_batches = NUM_BATCHES
    batch_size = BATCH_SIZE
    print("number of timesteps of data: ", num_epochs * num_runs_per_update * num_timesteps, " number of timestep updates: ", (num_epochs - randomly_choose_steps) * num_batches * batch_size)
    print("number of data saved until target averages into value of state for TD error (see if it is still in replay):", (1.0 / (1-POLYAK_CONSTANT)) * (num_timesteps/num_batches))

    target_Q_list, Q_list, Policy = set_all_networks(NETWORK_LAYERS, NETWORK_LAYER_SIZE, 3, 1) #was 256 when sorta seeming promisingS, 128 seemed promising as well
    #Policy.load_weights(currentdir + '/saved_models/model_set_%s' %1 + '/Policy')
    #Q_list[0].load_weights(currentdir + '/saved_models/model_set_%s' %1 + '/Q_1')
    #Q_list[1].load_weights(currentdir + '/saved_models/model_set_%s' %1 + '/Q_2')
    #target_Q_list[0].load_weights(currentdir + '/saved_models/model_set_%s' %1 + '/tQ_1')
    #target_Q_list[1].load_weights(currentdir + '/saved_models/model_set_%s' %1 + '/tQ_2')

    #conclusions:
    # lr can be higher but not seeing significant improvements
    # learns too much, with most data the Q-function knowing way too well
    # the policy doesn't seem to have enough data in the Q function to get a better action from
    # the entropy can be negative at the start with really low stds
    # too small network does seem to have detriments
    # Seriously seriously the policy never can learn from the Q function
    # the policy changes its action but that doesn't improve its loss
    # the policy drastically changes its action from sampling regardless if it is high LR or not, sampling is such high variance that its just no way to help loss

    env = gym.make('Pendulum-v1')
    s = env.reset()
    a = env.action_space.sample()
    s_prime, r, d, _ = env.step(a)
    replay = replay_buffer(20000, [s, a, r, s_prime, d])
    print(replay.data)

    #repeat
    for epochs in range(num_epochs): 
        print("epoch: ", epochs)
        #print("start")
        av_reward = 0.0
        for gather_data in range(num_runs_per_update):
            observation = env.reset()
            for timesteps in range(num_timesteps+1):
                if epochs < randomly_choose_steps:
                    action = env.action_space.sample()
                else:
                    action = sample_action(Policy, replay.list_to_data(observation)) #replay.list_to_data converts list in 2 ndim tensor

                    if epochs % RENDER_EPOCH == 0 and epochs > randomly_choose_steps:
                        print(action[0, 0])
                        print("action without noise:", sample_action(Policy, replay.list_to_data(observation), 0.0001))
                        env.render()
                        print("noise effect on action:", tf.abs(sample_action(Policy, replay.list_to_data(observation), 0.0001) - action[0, 0]))
                        expected_reward = Q_list[0](concatenate_state_action_Q(replay.list_to_data(state), replay.list_to_data(action)))
                        #print("expected reward:", expected_reward) #we want to be choosing high reward actions
                        print("difference between expected and real reward: ", expected_reward - replay.list_to_data(reward))
                    elif epochs % QUICK_RENDER_EPOCH == 0 and epochs != 0:
                        env.render()
                        combined_tensor = Policy(tf.expand_dims(tf.squeeze(observation), 0)) #outputs a [batch size, output vector size]
                        mean, log_std = tf.split(combined_tensor, 2, axis=1)
                        if timesteps % 10 == 0:
                            print("policy std:", tf.clip_by_value(log_std, LOGSTD_MIN, LOGSTD_MAX))
                    if timesteps % NUM_STEPS_PER_UPDATE_SET == 0 and timesteps != 0:
                        training_instance_anybs(Q_list, target_Q_list, Policy, replay.make_dataset(), num_batches, batch_size, 1)
                        train_target_models(Q_list, target_Q_list)

                state = observation
                observation, reward, done, _ = env.step(action * 2)

                replay.add_data([state, action, reward, observation, done])
                av_reward += float(reward) #tensorboard logging

                if done == True:
                    print("Episode finished after {} timesteps".format(timesteps+1))
                    break

        if epochs > randomly_choose_steps:
            if epochs % NET_EPOCH == 0:
                training_instance(Q_list, target_Q_list, Policy, replay.make_dataset(), num_batches, batch_size)
            Make_summary_info({'av_reward':(av_reward/float(num_runs_per_update * num_timesteps))}, writer, epochs) #tensorboard logging
            print("epoch done: ", epochs, " av_reward: ", av_reward/float(num_runs_per_update * num_timesteps))

    print("training complete in: ", (time.time() - start_time), " seconds")

    #now test with a render() and print average loss over one run
    for timesteps in range(200):
        env.render()
        action = sample_action(Policy, replay.list_to_data(observation), noise_std=0.0)
        state = observation
        observation, reward, done, _ = env.step(action)

        if done == True:
            print("Episode finished after {} timesteps".format(timesteps+1))
            break

    save_all_networks(Q_list, target_Q_list, Policy, 2)
    env.close()

def use_saved_policy():
    env = gym.make('Pendulum-v1')
    s = env.reset()
    a = env.action_space.sample()
    s_prime, r, d, _ = env.step(a)
    replay = replay_buffer(1000, [s, a, r, s_prime, d])

    #collecting Policy weights
    _, _, Policy = set_all_networks(3, 50, 3, 1)
    Policy.load_weights(currentdir + '/saved_models/model_set_%s' %0 + '/Policy')

    for ii in range(100):
        observation = env.reset()
        for timesteps in range(150):
            env.render()
            action = sample_action(Policy, replay.list_to_data(observation), noise_std=0.0)
            observation, _, done, _ = env.step(action)
            print(timesteps)
            if done == True:
                print("Episode finished after {} timesteps".format(timesteps+1))
                break

    env.close()

if __name__ == "__main__":
    main()
    #use_saved_policy()


    #what we see is spinning it over and over is the best over everything that we try including future rewards and everything
    #However, this may be due to us searching under the impression that a huge push around is a known good move. Knowing that we are going to at some point always do
    #the big spin, just continuing to do the big spin seems to be the winning decision
    #so the better idea is to learn the big spin knowing that it leads to poor futures earlier on
    #states-actions are all evaluated knowing what decisions we will be making, if we lock in that this idea isn't that bad because of the polyak
    #we then take everything else through the lens that we will be doing this poor action guaranteed and that makes other actions that could be good
    #without this action seem bad
    #with very high value negative actions, we want to know the future faster to know its incorrect unlike when with positive actions we want to know
    #the future slowly to have a lower estimation of how good the action is. Polyak with negative rewards overvalues the action and that is a stability killer
    #what can we do to say that the spinning decision is bad, how do we know its bad to do that large spinning?
    #another problem is that we are having too much noise. We want to choose which actions can have more noise than others
    #but too much entropy importance makes it so that we care more about choosing actions that have better results in entropy than high value actions
    #the polyak averaging makes it so that we first explore for a period of time with the concept that these actions have an unknown outcome
    #the truth is that we really don't know the action's outcome yet so they should be given the benefit of the doubt
    #for negative rewards, the actions that have bad later on will seem appealing at the start then we will learn their futures through testing
    #once an action is learned into the Q, they will be put into the stage through the policy.
    #once put onto the stage, they are given polyak steps of decaying benefit of the doubt, where we don't consider any states as linked or having a certain outcome
    #while this runs, we set in the real outcome into the Q, and the target networks learn this as they average in
    #we then siphon which actions are the best knowing their expected outcomes (indiscriminantly has all TD rewards go in, even for not tested much actions)
    #the policy begins to operate on the knowledge of the future outcomes so we can essentially blank out actions given state that have bad results
    #however, it does seem that we continue to run actions expecting bad results, the Q needs to be able to tell policy which action would do better from some small pool of tried
    #actions. 
    #without polyak, the Q-network will very quickly evaluate an action given what the future is, without considering that within that future there could be many different things
    #with polyak, for the first many steps it will seem like there is no understanding of future states as the acitons that lead to poor futures continue to run
    #small replay buffer means that we will end up training a lot on just the newer things. With our new understanding of things, we need to be able to go back
    #and see tons of different state-actions to get new insight, and speaking of generalization, figuring out a different state-action can help
    #policy learn the correct action for different states when learning something more irrelevant
    #A small replay buffer can really ruin everything thought as it will spend a ton of time learning the same stuff and when you only learn off
    #of what you just did that you don't have much loss from, you aren't learning much new except for the stochastic actions
    #learning old data with new TD errors is something to actually learn
    #tuning the noise and the entropy is really difficult, maybe have the noise aim for an average of abs(0.5) change
    #with very high noise, the std outputted becomes incorrect as we aren't using it as the randomization chance
    #we are instead using the noise_std so the chance of the action happening are all wrong, we need to keep this value at just 1
    #when we start using different noise, the unbelievably low action chance makes the Q evaluation become huge based on the entropy with -log
    #we start to just get completely wrong Q values due to massively random -log values in the entropy term, with action chance being like 3e-27, so -log(3e-27)
    #the only difference between SAC here and DDPG is that I lose exploration from repeatedly learning to lower the entropy for everything from gradients.
    #also the chance to take the action gets so low like e-100 from choosing actions with such a low chance to happen frequently, this explodes certain Q values
    #this also makes the policy learn to have a huge rate of change on the policy to lower the entropy by making the stds just forever lower
    #the wrong entropy calculation causes the entropy importance to overpower the network

    #it figures out the good decision almost instantly then decides that its poor and stops after training, gets worse after training
    #the big problem regardless of this is that the action without noise is almost always the same somehow
    #it doesn't know better actions and sometimes just stops trying to get better within ditches
    #then it will also get stuck doing the same action over and over with low exploration
    #the means when taught are just always wrong regardless if we have good exploration or not

    #the noise will constantly inflate it, so asks the mu to go lower? It makes it go lower in the target direction

    #with how random things are, we can't even see consistent progress because the different sampling of actions for TD reward and noise for policy Qs
