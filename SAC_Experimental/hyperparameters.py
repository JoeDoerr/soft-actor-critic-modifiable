#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Changeable hyperparameters for train.py and run.py
SCALE_E_INNER and SCALE_E_OUTER:
scaler = e^(SCALE_E_INNER * loss * -1) * SCALE_E_OUTER
SCALE_E_INNER=0.3
SCALE_E_OUTER=0.5
This will cause loss of -2.3 to reduce the entropy, past there exponentially get worse, with at loss -10 multiplying entropy by 10
This scaler scales the entropy term in the policy update
The idea is that poorly performing actions should have a boost in entropy further than SAC natural boosts

@author: Joe Doerr
"""

LOGSTD_MIN = -20.0
LOGSTD_MAX = 2.0

IND_NOISE_STD = 1.0

ALPHA = 0.9
#alpha=0.9
#scaling entropy=0.07
#to keep std but also keep mean being towards the real action
#also real action should learn more way later on
#find a way to increase the scale_d_loss as it 
#the deterministic updates towards what exactly the Q wants seem to always end up with very low action values of doing lots of nothing
#we should blow up entropy when rewards are low
SCALE_D_LOSS=0.10

#SCALE_E_INNER=0.38
#SCALE_E_OUTER=0.34
#SCALE_E_MIN=-10.0
#SCALE_E_MAX=0.0
SCALE_E_INNER=0.51
SCALE_E_OUTER=0.07
SCALE_E_MIN=-8.5
SCALE_E_MAX=0.0

GAMMA = 0.99
POLYAK_CONSTANT = 0.995
LEARNING_RATE = 0.0001

"""
Changeable hyperparameters for whichever RL file
"""
EPOCHS = 1500
RANDOMLY_CHOOSE_STEPS = 10
NUM_RUNS_PER_UPDATE = 1
TIMESTEPS = 200
NUM_STEPS_PER_UPDATE_SET = 99
START_UPDATING = 10
NUM_BATCHES = 50 #an insane amount of updates per step is supposedly good
BATCH_SIZE = 80

NETWORK_LAYERS = 2
NETWORK_LAYER_SIZE = 256

RENDER_EPOCH = 15
QUICK_RENDER_EPOCH = 3
NET_EPOCH = 15
