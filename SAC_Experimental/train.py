#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Uses build.py methods to train neural nets in SAC

@author: Joe Doerr
"""

import sys
import os
sys.path.append("../")
currentdir = os.path.dirname(os.path.realpath(__file__)) #macro folder
parentdir = os.path.dirname(currentdir) #example folder

import tensorflow as tf
import time

#other file includes
from prop_layers import nn_model
from SAC_Experimental.run import concatenate_state_action_Q, sample_action_with_entropy, sample_deterministic_action, sample_pre_tanh_action, entropy_of_pre_tanh_action
from tflog import Make_summary_info
from SAC_Experimental.hyperparameters import *

learning_rate=tf.Variable(LEARNING_RATE,trainable=False,name='learning_rate') #0.0001 was before
optimizer = tf.keras.optimizers.Adam(learning_rate)

#send to training_instance_anybs to then call train_real_models which will apply updates
def training_instance_anybs(Q_model_list, target_Q_list, policy_model, replay_dataset, update_iters:int, batch_size:int, scale_to_fit:int = 10):
  """
  We divide batch size by 10 so that we can put a large batch size in and still can fit on the gpu
  We run the batch sizes from the whole train_dataset and sum them up until 10 and apply each time

  For Q networks:
  for each minibatch calculate TD error-> get_TD_target, pass into train_real_Qs to get gradient for update
  At end average gradients by scale_to_fit, apply to network
  For policy:
  for each minibatch, maximize expected value of Q and entropy with train_policy, get gradients for update
  At end average gradients by scale_to_fit, apply to network
  """
  num_models = 3
  use_policy = 2
  batch_size /= scale_to_fit
  replay_dataset = replay_dataset.batch(int(batch_size))
  for mm in range(num_models):
    #print("model number:", mm)
    updates = 0
    sum_batch_num = 0
    grads = []
    TD_target_tensor = -5

    for minibatch in replay_dataset:
      # Get TD Target
      TD_target_tensor = get_TD_target(target_Q_list, policy_model, minibatch) #ensures only runs once for both Qs
      # Which model used
      model = policy_model # Which model to run this for
      if mm < use_policy: model = Q_model_list[mm]

      # Getting one minibatch gradient
      if sum_batch_num == 0: #~~~start the grads = 
        if mm < use_policy: grads = train_real_Qs(TD_target_tensor, model, minibatch, batch_size)
        else:                          grads = train_policy(policy_model, Q_model_list, minibatch, batch_size)
      else:                  #~~~work on the exist grads, grads_add = 
        if mm < use_policy: grads_add = train_real_Qs(TD_target_tensor, model, minibatch, batch_size)
        else:                          grads_add = train_policy(model, Q_model_list, minibatch, batch_size)
        for sg in range(len(grads)): #for each tensor in the list, tf.add them together
          grads[sg] = tf.add(grads[sg], grads_add[sg])
      sum_batch_num += 1
      
      # Averaging minibatch gradients when "scale_to_fit" number of minibatches run, then applies averaged gradient
      if sum_batch_num == scale_to_fit: # ~ When completed the full batch size, make the gradients the average
        sum_batch_num = 0
        divide_by = tf.constant([float(scale_to_fit)], dtype=tf.float64)
        for sg in range(len(grads)): #for each tensor in the list, tf.divide them by the number of grads added together
          grads[sg] = tf.divide(grads[sg], divide_by)

          if tf.reduce_any(tf.math.is_nan(grads[sg])) == True:
            print("gradient nan")

        #print("model number updated:", mm)
        optimizer.apply_gradients(zip(grads, model.trainable_variables))
        updates += 1
        if updates >= update_iters:
          break #when we have done enough batches
  """
  training complete
  """

#for seeing if the network is training
def training_instance(Q_model_list, target_Q_list, policy_model, replay_dataset, update_iters:int, batch_size:int):
  num_models = 3
  use_policy = 2
  update_iters = 1
  replay_dataset = replay_dataset.batch(int(batch_size))
  for mm in range(num_models):
    updates = 0
    for minibatch in replay_dataset:
      # Get TD Target
      TD_target_tensor = get_TD_target(target_Q_list, policy_model, minibatch) #ensures only runs once for both Qs
      # Which model used
      model = policy_model # Which model to run this for
      if mm < use_policy: model = Q_model_list[mm]
      if mm < use_policy: grads = train_real_Qs(TD_target_tensor, model, minibatch, batch_size)
      else:               grads = train_policy(policy_model, Q_model_list, minibatch, batch_size)

      if mm < use_policy:
        s, a, _, _, _ = minibatch
        out_tensor = model(concatenate_state_action_Q(s, a)) # ~ model can take tensor as input and output tensor of [batch size, output vector]
        # MSBE between two tensors that have each element as a datapoint, average the losses together then use as minimization target
        loss = tf.math.square(tf.cast(out_tensor, dtype=tf.float64) - TD_target_tensor)
        print("Q loss before update:", loss[1, 0])
      else:
        states_tensor, _, _, _, _ = minibatch #tensor of: [batchsize, statetensorsize] for states_tensor
        actions_tensor, entropy_tensor = sample_action_with_entropy(policy_model, states_tensor)
        input = concatenate_state_action_Q(states_tensor, actions_tensor) #concatenate with states_tensor for input to Q functions
        loss = Q_model_list[0](input)
        loss = loss + (ALPHA * entropy_tensor)
        print("Policy value we want to increase before update:", loss[1, 0], "entropy term is this much: ", (ALPHA * entropy_tensor)[1, 0], "\naction: ", actions_tensor[1, 0])
        actions_tensor, entropy_tensor = sample_action_with_entropy(policy_model, states_tensor)
        print("sample another action:, ", actions_tensor[1, 0])

      optimizer.apply_gradients(zip(grads, model.trainable_variables))

      if mm < use_policy:
        s, a, _, _, _ = minibatch

        out_tensor = model(concatenate_state_action_Q(s, a)) # ~ model can take tensor as input and output tensor of [batch size, output vector]
        loss = tf.math.square(TD_target_tensor - tf.cast(out_tensor, dtype=tf.float64))
        print("Q loss after update:", loss[1, 0])
      else:
        states_tensor, _, _, _, _ = minibatch #tensor of: [batchsize, statetensorsize] for states_tensor
        actions_tensor, entropy_tensor = sample_action_with_entropy(policy_model, states_tensor)
        input = concatenate_state_action_Q(states_tensor, actions_tensor) #concatenate with states_tensor for input to Q functions
        loss = Q_model_list[0](input)
        loss = loss + (ALPHA * entropy_tensor)
        print("Policy value we want to increase after  update:", loss[1, 0], "entropy term is this much: ", (ALPHA * entropy_tensor)[1, 0], "\naction: ", actions_tensor[1, 0])

      #optimizer.apply_gradients(zip(grads, model.trainable_variables))
      updates += 1
      if updates >= update_iters:
        break #when we have done enough batches
  """
  training complete
  """

def get_TD_target(target_Q_list, policy_model, minibatch): #remakes itself each time alpha constant changes
  _, _, reward, n_states_tensor, d = minibatch
  #sample action from current policy using its normal decision making functionality
  actions_tensor, entropy_tensor = sample_action_with_entropy(policy_model, n_states_tensor)
  input = concatenate_state_action_Q(n_states_tensor, actions_tensor)

  #get lower target Q function for each s, a, using tf.math.minimum between the two output tensors
  tQ_out_0 = target_Q_list[0](input)
  tQ_out_1 = target_Q_list[1](input)
  TD_target = tf.math.minimum(tQ_out_0, tQ_out_1)
  #print("min: ", TD_target)
  TD_target = TD_target + (ALPHA * entropy_tensor)
  #print("epn: ", TD_target)
  #scale using gamma and d
  TD_target = tf.cast(TD_target, dtype=tf.float64) * (GAMMA * (1.0-d))
  #add reward then return the tensor
  return tf.cast(TD_target, dtype=tf.float64) + reward

#Here is the calculation of the updating formulation of the loss functions and such
#pass in real_model_list, target_model_list, minibatch from the replay memory dataset -> we have the use of replay memory dataset updating method in here
@tf.function
def train_real_Qs(TD_target, Q_model, minibatch, batch_size):
  with tf.GradientTape() as tape:
    s, a, _, _, _ = minibatch
    out_tensor = Q_model(concatenate_state_action_Q(s, a)) # ~ model can take tensor as input and output tensor of [batch size, output vector]
    # MSBE between two tensors that have each element as a datapoint, average the losses together then use as minimization target
    loss = tf.math.square(TD_target - tf.cast(out_tensor, dtype=tf.float64))
    loss /= batch_size
    #print("Q_loss: ", tf.reduce_sum(tf.abs(loss)))
    grads = tape.gradient(target=loss, sources=Q_model.trainable_variables)
    return grads

@tf.function
def train_policy(policy_model, Q_model_list, minibatch, batch_size):
  with tf.GradientTape() as tape:
    states_tensor, _, _, _, _ = minibatch #tensor of: [batchsize, statetensorsize] for states_tensor
    actions_tensor, entropy_tensor = sample_action_with_entropy(policy_model, states_tensor)
    input = concatenate_state_action_Q(states_tensor, actions_tensor) #concatenate with states_tensor for input to Q functions

    #apply to both Q_models then tf.math.minimum to find the minimum for each datapoint
    Q_out_0 = Q_model_list[0](input)
    Q_out_1 = Q_model_list[1](input)
    loss = tf.math.minimum(Q_out_0, Q_out_1)
    #loss *= 0.00001

    loss = loss + (ALPHA * entropy_tensor) #entropy can be negative when there are super low stds
    loss = loss / batch_size

    # we want to perform gradient ascent but the optimizer always minimizes, make negative to achieve
    loss *= -1.0
    grads = tape.gradient(target=loss, sources=policy_model.trainable_variables)
    return grads

import math
@tf.function
def depreciated_train_policy(policy_model, Q_model_list, minibatch, batch_size):
  states_tensor, _, _, _, _ = minibatch #tensor of: [batchsize, statetensorsize] for states_tensor =

  #should get the same std for everyone and same mean for everyone, get one version inside, get one version outside, sample the same noise
  combined_tensor_o = policy_model(states_tensor) #outputs a [batch size, output vector size]
  mean_o, log_std_o = tf.split(combined_tensor_o, 2, axis=1)
  log_std_o = tf.clip_by_value(log_std_o, LOGSTD_MIN, LOGSTD_MAX)
  std_o = tf.exp(log_std_o) #e^log_std
  o = tf.random.normal(std_o.shape, 0, IND_NOISE_STD, dtype=tf.float64)
  pta_o = mean_o + (o * std_o)
  entropy_scale_d_loss = entropy_of_pre_tanh_action(pta_o, mean_o, std_o)
  actions_tensor_e = tf.math.tanh(pta_o)

  input_e = concatenate_state_action_Q(states_tensor, actions_tensor_e) #concatenate with states_tensor for input to Q functions
  Q_out_0_e = Q_model_list[0](input_e)
  Q_out_1_e = Q_model_list[1](input_e)
  loss_e = tf.math.minimum(Q_out_0_e, Q_out_1_e)
  loss_e = tf.clip_by_value(loss_e, SCALE_E_MIN, SCALE_E_MAX)
  e_scaler = tf.math.exp(loss_e * SCALE_E_INNER * -1) * SCALE_E_OUTER #negative loss scales it up, positive loss scales it down, even if low won't be less than e^0=1
  #print("escaler:", e_scaler[0, 0], " loss: ", loss_e[0, 0]) 

  #It should be actions with bad rewards  -> STD+
  #It should be actions with good rewards -> STD-

  pta, mean, std = sample_pre_tanh_action(policy_model, states_tensor)
  with tf.GradientTape() as tape:
    #all this just so we can share the same noise as things we want to backpropagate to and things we don't
    combined_tensor = policy_model(states_tensor)
    mean, log_std = tf.split(combined_tensor, 2, axis=1)
    log_std = tf.clip_by_value(log_std, LOGSTD_MIN, LOGSTD_MAX)
    std = tf.exp(log_std) #e^log_std
    pta = mean + (o * std)
    actions_tensor = tf.math.tanh(pta)

    input = concatenate_state_action_Q(states_tensor, actions_tensor) #concatenate with states_tensor for input to Q functions
    #apply to both Q_models then tf.math.minimum to find the minimum for each datapoint
    Q_out_0 = Q_model_list[0](input)
    Q_out_1 = Q_model_list[1](input)
    loss = tf.math.minimum(Q_out_0, Q_out_1)

    pta_use = mean_o + (o * std) #only backpropagate to the std
    pta_use = tf.math.tanh(pta_use)
    entropy_tensor = entropy_of_pre_tanh_action(pta, mean, std)
    #e_scaler = 1.0
    #want it so that stds scale appropriately against the push to reduce stds, have it increase the rate of change as it decreases
    #loss = loss + (ALPHA * e_scaler * entropy_tensor * (tf.math.pow(tf.cast(0.87, dtype=tf.float64), std_o) * 0.5)) #entropy can be negative when there are super low stds
    #loss = loss / batch_size
    # we want to perform gradient ascent but the optimizer always minimizes, make negative to achieve

    #at certain level of reward, 3, reduce std by multiplying by negative
    #at over 3, increase std by certain level of positive
    #Keep it slow to change
    #loss *= 0.001
    loss += (entropy_tensor * ALPHA * 0.5 * ((-8.0 * tf.tanh(0.2 * loss_e))-5.312)) #at -4 reward it becomes negative so reduces the std
    #loss += (entropy_tensor * ALPHA * 0.00030 * (-1.0 * tf.math.exp(0.1 * std_o)) + 1.0) #reduce entropy when at 0 to 2, increase entropy when at -20 to 0, keeps std from straying
    loss *= -1.0

    #after loss is calcualted, want to independently fix mean to go in the correct direction on its own
    #no noise mean going to the correct action will eventually cause the std to reduce as it is always at the correct mean
    d_action = sample_deterministic_action(policy_model, states_tensor)
    d_input = concatenate_state_action_Q(states_tensor, d_action)
    Q_out_0 = Q_model_list[0](d_input)
    Q_out_1 = Q_model_list[1](d_input)
    d_loss = tf.math.minimum(Q_out_0, Q_out_1)
    d_loss *= -1.0

    #training the mean correctly fast is unstable thats why we see target policy in DDPG
    #here, we need to have the policy learn delayed, learning too fast breaks it
    total_loss = loss + (d_loss * (SCALE_D_LOSS) * tf.math.exp(((-8.0 * tf.tanh(0.2 * loss_e))-5.312) / -2.0)) #scale d_term down, higher the entropy value the higher we scale itdown

    grads = tape.gradient(target=total_loss, sources=policy_model.trainable_variables)
    #del tape
    return grads
    
def train_target_models(Q_model_list, target_Q_list):
  #for loop to each layer we scale the tensors and add them
  for mm in range(len(Q_model_list)):
    for dl in range(len(Q_model_list[mm].dense_layers)):
      target_Q_list[mm].dense_layers[dl].w = (target_Q_list[mm].dense_layers[dl].w * POLYAK_CONSTANT) + (Q_model_list[mm].dense_layers[dl].w * (1.0-POLYAK_CONSTANT))
      target_Q_list[mm].dense_layers[dl].b = (target_Q_list[mm].dense_layers[dl].b * POLYAK_CONSTANT) + (Q_model_list[mm].dense_layers[dl].b * (1-POLYAK_CONSTANT))
    target_Q_list[mm].output_layer.w = (target_Q_list[mm].output_layer.w * POLYAK_CONSTANT) + (Q_model_list[mm].output_layer.w * (1.0-POLYAK_CONSTANT))
    target_Q_list[mm].output_layer.b = (target_Q_list[mm].output_layer.b * POLYAK_CONSTANT) + (Q_model_list[mm].output_layer.b * (1.0-POLYAK_CONSTANT))
  return