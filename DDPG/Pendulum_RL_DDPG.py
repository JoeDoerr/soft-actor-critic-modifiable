#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Methods to run through RL environments using SAC
replay_buffer, sample_action, concatenate_state_action_Q, action_chance

@author: Joe Doerr
"""

import sys

from tensorflow._api.v2 import random
sys.path.append("../")
import tensorflow as tf
import gym
import numpy as np
import time
import math
import os
currentdir = os.path.dirname(os.path.realpath(__file__)) # dir to folder holding this file

#includes from own files:
from run_DDPG import replay_buffer, sample_action_explore, save_all_networks, set_all_networks, OUNoise
from train_DDPG import training_instance, train_target_models, concatenate_state_action_Q
from tflog import Make_summary_info
from hyperparameters_DDPG import *

log_path=currentdir+'/'+'logs'
writer = tf.summary.create_file_writer(log_path)

class ActionNormalizer(gym.ActionWrapper):
    """Rescale and relocate the actions."""

    def action(self, action: np.ndarray) -> np.ndarray:
        """Change the range (-1, 1) to (low, high)."""
        low = self.action_space.low
        high = self.action_space.high

        scale_factor = (high - low) / 2
        reloc_factor = high - scale_factor

        action = action * scale_factor + reloc_factor
        action = np.clip(action, low, high)

        return action

    def reverse_action(self, action: np.ndarray) -> np.ndarray:
        """Change the range (low, high) to (-1, 1)."""
        low = self.action_space.low
        high = self.action_space.high

        scale_factor = (high - low) / 2
        reloc_factor = high - scale_factor

        action = (action - reloc_factor) / scale_factor
        action = np.clip(action, -1.0, 1.0)

        return action

#setup, network size is important for SAC, small network can cause it not to work
def main():
    model_set=0
    start_time = time.time()
    randomly_choose_steps = RANDOMLY_CHOOSE_STEPS
    num_runs_per_update = NUM_RUNS_PER_UPDATE
    num_timesteps = TIMESTEPS
    num_epochs = EPOCHS
    num_batches = NUM_BATCHES
    batch_size = BATCH_SIZE
    print("number of timesteps of data: ", num_epochs * num_runs_per_update * num_timesteps, " number of timestep updates: ", (num_epochs - randomly_choose_steps) * num_batches * batch_size)
    print("number of data saved until target averages into value of state for TD error (see if it is still in replay):", (1.0 / (1-POLYAK_CONSTANT)) * (num_timesteps/num_batches))

    targ_Q, Q_model, Policy, targ_Policy = set_all_networks(NETWORK_LAYERS, NETWORK_LAYER_SIZE, 3, 1) #was 256 when sorta seeming promisingS, 128 seemed promising as well
    #Policy.load_weights(currentdir + '/saved_models/model_set_%s' %model_set + '/P')
    #targ_Policy.load_weights(currentdir + '/saved_models/model_set_%s' %model_set + '/tP')
    #Q_model.load_weights(currentdir + '/saved_models/model_set_%s' %model_set + '/Q')
    #targ_Q.load_weights(currentdir + '/saved_models/model_set_%s' %model_set + '/tQ')

    #conclusions:
    # lr can be higher but not seeing significant improvements
    # learns too much, with most data the Q-function knowing way too well
    # the policy doesn't seem to have enough data in the Q function to get a better action from
    # the entropy can be negative at the start with really low stds
    # too small network does seem to have detriments
    # Seriously seriously the policy never can learn from the Q function
    # the policy changes its action but that doesn't improve its loss
    # the policy drastically changes its action from sampling regardless if it is high LR or not, sampling is such high variance that its just no way to help loss

    step = 0
    env = gym.make("Pendulum-v1")
    env = ActionNormalizer(env)
    print(env.action_space)
    print(env.observation_space)
    s = env.reset()
    a = env.action_space.sample()
    s_prime, r, d, _ = env.step(a)
    replay = replay_buffer(100000, [s, a, r, s_prime, d])
    print(replay.data)
    ou_noise = OUNoise(env.action_space.shape[0], 0, OUNOISE_THETA, OUNOISE_SIGMA)

    #repeat
    for epochs in range(num_epochs): 
        print("epoch: ", epochs)
        #if epochs % 100 == 0 and epochs != 0:
            #randomly_choose_steps = epochs + RANDOMLY_CHOOSE_STEPS_MID
        av_reward = 0.0
        for gather_data in range(num_runs_per_update):
            observation = env.reset()
            for timesteps in range(num_timesteps+1):
                if epochs < randomly_choose_steps:
                    action = env.action_space.sample()

                else:
                    action = sample_action_explore(Policy, replay.list_to_data(observation), ou_noise) #replay.list_to_data converts list in 2 ndim tensor

                    if epochs % RENDER_EPOCH == 0 and epochs > randomly_choose_steps:
                        print("action without noise:", Policy(replay.list_to_data(observation)))
                        env.render()
                        print("noise effect on action:", tf.abs((Policy(replay.list_to_data(observation)) - action[0, 0])))
                        #expected_reward = Q_model(concatenate_state_action_Q(replay.list_to_data(state), replay.list_to_data(action)))
                        #print("difference between expected and real reward: ", expected_reward - replay.list_to_data(reward))
                    
                    if timesteps % NUM_STEPS_PER_UPDATE_SET == 0:
                        step = training_instance(Q_model, targ_Q, Policy, targ_Policy, replay.make_dataset(), num_batches, batch_size, step)

                state = observation
                observation, reward, done, _ = env.step(action)

                replay.add_data([state, action, reward, observation, done])
                av_reward += float(reward) #tensorboard logging

                if done == True:
                    print("Episode finished after {} timesteps".format(timesteps+1))
                    break

        if epochs > randomly_choose_steps:
            Make_summary_info({'av_reward':(av_reward/float(num_runs_per_update * num_timesteps))}, writer, epochs - RANDOMLY_CHOOSE_STEPS) #tensorboard logging
            print("epoch done: ", epochs, " av_reward: ", av_reward/float(num_runs_per_update * num_timesteps))

    print("training complete in: ", (time.time() - start_time), " seconds")
    #save_all_networks(Q_list, target_Q_list, Policy, 2)
    env.close()
    save_all_networks(Q_model, targ_Q, Policy, targ_Policy, model_set)


def use_saved_policy():
    env = gym.make('Pendulum-v1')
    s = env.reset()
    a = env.action_space.sample()
    s_prime, r, d, _ = env.step(a)
    replay = replay_buffer(1000, [s, a, r, s_prime, d])
    ou_noise = OUNoise(env.action_space.shape[0], 0, OUNOISE_THETA, OUNOISE_SIGMA)

    #collecting Policy weights
    _, _, Policy, _ = set_all_networks(NETWORK_LAYERS, NETWORK_LAYER_SIZE, 3, 1)
    Policy.load_weights(currentdir + '/saved_models/model_set_%s' %90 + '/P')

    for ii in range(100):
        observation = env.reset()
        for timesteps in range(TIMESTEPS+1):
            env.render()
            action = sample_action_explore(Policy, replay.list_to_data(observation), ou_noise)
            observation, _, done, _ = env.step(action)
            print(timesteps)
            if done == True:
                print("Episode finished after {} timesteps".format(timesteps+1))
                break

    env.close()

if __name__ == "__main__":
    #main()
    use_saved_policy()