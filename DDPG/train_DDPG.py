#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Uses build.py methods to train neural nets in SAC

@author: Joe Doerr
"""

import sys
import os
sys.path.append("../")
currentdir = os.path.dirname(os.path.realpath(__file__)) #macro folder
parentdir = os.path.dirname(currentdir) #example folder

import tensorflow as tf
import time

#other file includes
from prop_layers import nn_model
from DDPG.run_DDPG import concatenate_state_action_Q
from tflog import Make_summary_info
from hyperparameters_DDPG import *

learning_rate=tf.Variable(3e-4,trainable=False,name='learning_rate') #0.0001 was before
optimizer = tf.keras.optimizers.Adam(learning_rate)
learning_rate_Q=tf.Variable(1e-3,trainable=False,name='learning_rate') #0.0001 was before
optimizer_Q = tf.keras.optimizers.Adam(learning_rate_Q)
log_path=currentdir+'/'+'logs'
writer = tf.summary.create_file_writer(log_path)
step=0

def training_instance(Q_model, target_Q, policy_model, target_policy_model, replay_dataset, update_iters:int, batch_size:int, step):

  replay_dataset = replay_dataset.batch(int(batch_size))
  ups = 0
  avl_Q=0.0
  avl_P=0.0
  for minibatch in replay_dataset:
      # Get TD Target
      TD_target_tensor = get_TD_target(target_Q, target_policy_model, minibatch)

      """
      print("TD_target: ", TD_target_tensor)
      _, _, reward, n_states_tensor, d = minibatch
      actions_tensor = policy_model(n_states_tensor)
      tactions_tensor = target_policy_model(n_states_tensor)
      tinput = concatenate_state_action_Q(n_states_tensor, tactions_tensor)
      input = concatenate_state_action_Q(n_states_tensor, actions_tensor)
      tdreal_evaluation = target_Q(tinput)
      tdreal_evaluation *= GAMMA * (1.0-d)
      print("Diff TD_target: ", tdreal_evaluation + reward)
      treal_evaluation = Q_model(tinput)
      treal_evaluation *= GAMMA * (1.0-d)
      print("Target using non-targ Q and targ Policy: ", treal_evaluation + reward)
      real_evaluation = Q_model(input)
      real_evaluation *= GAMMA * (1.0-d)
      print("Target using non-targ functions: ", real_evaluation + reward)

      print("without reward:")
      print("TD_target: ", TD_target_tensor - reward)
      print("Diff TD_target: ", tdreal_evaluation)
      print("Target using non-targ Q and targ Policy: ", treal_evaluation)
      print("Target using non-targ functions: ", real_evaluation)
      """

      grads, loss = train_real_Q(TD_target_tensor, Q_model, minibatch, batch_size)
      optimizer_Q.apply_gradients(zip(grads, Q_model.trainable_variables))
      avl_Q+=tf.reduce_mean(loss)
      
      grads, loss = train_policy(policy_model, Q_model, minibatch, batch_size)
      optimizer.apply_gradients(zip(grads, policy_model.trainable_variables))
      avl_P+=tf.reduce_mean(loss)

      #print("outside after update tQ: ", (target_Q.dense_layers[0].w)[0, 0])
      #real_evaluation = target_Q(input)
      #print("should be same tQ: ", real_evaluation)
      #real_evaluation = Q_model(input)
      #print("should be same Q: ", real_evaluation)

      ups += 1
      
      if ups > update_iters:
        break
    
  #each time it runs batches, update once
  train_target_models(Q_model, target_Q, policy_model, target_policy_model)

  Make_summary_info({'Q_loss':avl_Q/float(ups)}, writer, step)
  Make_summary_info({'Policy_loss':avl_P/float(ups)}, writer, step)
  step += 1

  return step
  """
  training complete
  """

#making this a @tf.function glitches the computation very badly
def get_TD_target(target_Q, target_policy_model, minibatch): #remakes itself each time alpha constant changes
  """
  _, _, reward, n_states_tensor, d = minibatch
  #sample action from current policy using its normal decision making functionality
  actions_tensor = target_policy_model(n_states_tensor)
  input = concatenate_state_action_Q(n_states_tensor, actions_tensor)

  TD_target = target_Q(input)
  TD_target = TD_target * GAMMA * (1.0-d)
  TD_target = TD_target + reward
  """
  _, _, reward, n_states_tensor, d = minibatch

  tactions_tensor = target_policy_model(n_states_tensor)
  tinput = concatenate_state_action_Q(n_states_tensor, tactions_tensor)

  td = target_Q(tinput)
  td *= GAMMA * (1.0-d)
  td += reward
  #print("Diff TD_target: ", td )
  return td

#Here is the calculation of the updating formulation of the loss functions and such
#pass in real_model_list, target_model_list, minibatch from the replay memory dataset -> we have the use of replay memory dataset updating method in here
@tf.function
def train_real_Q(TD_target, Q_model:nn_model, minibatch, batch_size):
  with tf.GradientTape() as tape:
    s, a, _, _, _ = minibatch
    out_tensor = Q_model(concatenate_state_action_Q(s, a))
    loss = tf.math.square(TD_target - tf.cast(out_tensor, dtype=tf.float64))
    loss /= batch_size
    grads = tape.gradient(target=loss, sources=Q_model.trainable_variables)
    return grads, loss

@tf.function
def train_policy(policy_model:nn_model, Q_model:nn_model, minibatch, batch_size):
  with tf.GradientTape() as tape:
    states_tensor, _, _, _, _ = minibatch #tensor of: [batchsize, statetensorsize] for states_tensor
    actions_tensor = policy_model(states_tensor)
    input = concatenate_state_action_Q(states_tensor, actions_tensor) #concatenate with states_tensor for input to Q functions
    loss =  Q_model(input)
    loss = loss / batch_size
    # we want to perform gradient ascent but the optimizer always minimizes, make negative to achieve
    loss *= -1.0
    grads = tape.gradient(target=loss, sources=policy_model.trainable_variables)
    return grads, loss
    
def train_target_models(Q, tQ, P, tP):
  #for loop to each layer we scale the tensors and add them
  #print("scaled tQ: ", (tQ.dense_layers[0].w)[0, 0])
  #print("scaled Q: ", (Q.dense_layers[0].w * (1.0-POLYAK_CONSTANT))[0, 0])
  #print("together Q: ", (tQ.dense_layers[0].w * POLYAK_CONSTANT) + (Q.dense_layers[0].w * (1.0-POLYAK_CONSTANT))[0, 0])
  for dl in range(len(Q.dense_layers)):
    tQ.dense_layers[dl].w = (tQ.dense_layers[dl].w * POLYAK_CONSTANT) + (Q.dense_layers[dl].w * (1.0-POLYAK_CONSTANT))
    tQ.dense_layers[dl].b = (tQ.dense_layers[dl].b * POLYAK_CONSTANT) + (Q.dense_layers[dl].b * (1.0-POLYAK_CONSTANT))
  tQ.output_layer.w = (tQ.output_layer.w * POLYAK_CONSTANT) + (Q.output_layer.w * (1.0-POLYAK_CONSTANT))
  tQ.output_layer.b = (tQ.output_layer.b * POLYAK_CONSTANT) + (Q.output_layer.b * (1.0-POLYAK_CONSTANT))
  #print("after update tQ: ", (tQ.dense_layers[0].w)[0, 0])

  for dl in range(len(P.dense_layers)):
    tP.dense_layers[dl].w = (tP.dense_layers[dl].w * POLYAK_CONSTANT) + (P.dense_layers[dl].w * (1.0-POLYAK_CONSTANT))
    tP.dense_layers[dl].b = (tP.dense_layers[dl].b * POLYAK_CONSTANT) + (P.dense_layers[dl].b * (1.0-POLYAK_CONSTANT))
  tP.output_layer.w = (tP.output_layer.w * POLYAK_CONSTANT) + (P.output_layer.w * (1.0-POLYAK_CONSTANT))
  tP.output_layer.b = (tP.output_layer.b * POLYAK_CONSTANT) + (P.output_layer.b * (1.0-POLYAK_CONSTANT))

  #so this does mathematically work here, but somehow after it changes
  #passing the tQ and tP out makes it change but not to the current value and not to the original

  return