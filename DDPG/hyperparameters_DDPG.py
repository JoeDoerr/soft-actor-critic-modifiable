#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Changeable hyperparameters for train.py and run.py

@author: Joe Doerr
"""

OUNOISE_THETA=1.0
OUNOISE_SIGMA=0.1
NOISE = 0.30
GAMMA = 0.99
POLYAK_CONSTANT = 0.995
LEARNING_RATE = 0.0001

"""
Changeable hyperparameters for whichever RL file
"""
EPOCHS = 80
RANDOMLY_CHOOSE_STEPS = 50
RANDOMLY_CHOOSE_STEPS_MID = 3
NUM_RUNS_PER_UPDATE = 1
TIMESTEPS = 200
NUM_STEPS_PER_UPDATE_SET = 1
NUM_BATCHES = 1 #an insane amount of updates per step is supposedly good
BATCH_SIZE = 50

NETWORK_LAYERS = 3
NETWORK_LAYER_SIZE = 128

RENDER_EPOCH = 20
NET_EPOCH = 1000