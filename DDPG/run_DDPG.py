#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Methods to run through RL environments using SAC
replay_buffer, sample_action, concatenate_state_action_Q, action_chance

@author: Joe Doerr
"""

import sys
from threading import active_count
sys.path.append("../")
import tensorflow as tf
from prop_layers import nn_model
from hyperparameters_DDPG import *

#replay buffer class, function to add new data and release old data, shuffle by full size
#keep unshuffled dataset that we keep manipulating then always pass in a newly shuffled copy
class replay_buffer():
    def __init__(self, size, new_data_list):
        self.size = size
        #data kept in tensors, we make these variables so we can more easily specifically access datapoints
        self.state = self.list_to_data(new_data_list[0])
        self.action = self.list_to_data(new_data_list[1])
        self.reward = self.list_to_data(new_data_list[2])
        self.n_state = self.list_to_data(new_data_list[3])
        self.d = self.list_to_data(float(new_data_list[4]))
        self.data = [self.state, self.action, self.reward, self.n_state, self.d]

    def list_to_data(self, input):
        tensor = tf.expand_dims(tf.squeeze(tf.convert_to_tensor(input)), 0) #make the batch dimension on axis=0
        if len(tensor.shape) == 1:
            tensor = tf.expand_dims(tensor, 1)
        return tf.cast(tensor, dtype=tf.float64)

    def add_data(self, new_data_list): #state, action, reward, n_state, d
        #new_data_list[4] = float(new_data_list[4]) #ensuring that d is an float
        for ii in range(len(self.data)):
            self.data[ii] = tf.concat([self.data[ii], self.list_to_data(new_data_list[ii])], 0) #concat along batch dimension
            if self.data[ii].shape[0] >= self.size:
                self.data[ii] = self.data[ii][1:, :] #taking off the oldest piece of data
    
    def make_dataset(self):
        #make with tensorslices then shuffle then return
        train_dataset=tf.data.Dataset.from_tensor_slices((self.data[0], self.data[1], self.data[2], self.data[3], self.data[4])).shuffle(self.size)
        return train_dataset

def concatenate_state_action_Q(states_tensor, actions_tensor):
    # shape of states_tensor and actions_tensor [batch size, tensor of given datapoint]
    sa = tf.concat([tf.cast(states_tensor, dtype=tf.float64), tf.cast(actions_tensor, dtype=tf.float64)], 1) #axis = 1 as we aren't adding the tensors to the same batch size axis=0, but putting the tensors together at each batch
    return sa

import random
import copy
import numpy as np

class OUNoise:
    """Ornstein-Uhlenbeck process.
    Taken from Udacity deep-reinforcement-learning github repository:
    https://github.com/udacity/deep-reinforcement-learning/blob/master/
    ddpg-pendulum/ddpg_agent.py
    """

    def __init__(
        self, 
        size: int, 
        mu: float = 0.0, 
        theta: float = 0.15, 
        sigma: float = 0.2,
    ):
        """Initialize parameters and noise process."""
        self.state = np.float64(0.0)
        self.mu = mu * np.ones(size)
        self.theta = theta
        self.sigma = sigma
        self.reset()

    def reset(self):
        """Reset the internal state (= noise) to mean (mu)."""
        self.state = copy.copy(self.mu)

    def sample(self) -> np.ndarray:
        """Update internal state and return it as a noise sample."""
        x = self.state
        dx = self.theta * (self.mu - x) + self.sigma * np.array(
            [random.random() for _ in range(len(x))]
        )
        self.state = x + dx
        return self.state

import numpy as np
#for quick environment action sampling
def sample_action_explore(policy_model, states_tensor, ou_noise:OUNoise):
    action = policy_model(states_tensor) #outputs a [batch size, output vector size]
    #o = tf.random.normal(action.shape, 0, noise_std, dtype=tf.float64)
    #noise = noise_scale * np.random.randn(action.shape[0], action.shape[1])
    noise = ou_noise.sample()
    action = action + (noise)
    action = tf.clip_by_value(action, -1, 1)

    return action


def log_avoid_inf_nan(input): #simply does log(input)
  if tf.reduce_any(tf.math.is_nan(tf.math.log(input))) == True or tf.reduce_any(tf.math.is_inf(tf.math.log(input))) == True:
    print("nan or inf from log, however we fixed it")
    return tf.math.log(input + 0.00000001)
  else:
    return tf.math.log(input) #NEGATIVES AND 0 MAKES LOG NAN


def set_all_networks(layer_num_all, layer_size_all, state_size, action_size): #no output activation function for any
    #(self, numLayers, input_size, layer_size, output_size, relu = True, tanh = False, softmax_out = False, tanh_out = True):
    Q_targ = nn_model(layer_num_all, (state_size + action_size), layer_size_all, 1, relu=True, tanh=False, softmax_out=False, tanh_out=False, name="Q_targ")
    Q = nn_model(layer_num_all, (state_size + action_size), layer_size_all, 1, relu=True, tanh=False, softmax_out=False, tanh_out=False, name="Q_ta")
    Policy = nn_model(layer_num_all, state_size, layer_size_all, action_size, relu=True, tanh=False, softmax_out=False, tanh_out=True, name="Policy")
    Policy_targ = nn_model(layer_num_all, state_size, layer_size_all, action_size, relu=True, tanh=False, softmax_out=False, tanh_out=True, name="Policy_targ")
    """
    for dl in range(len(Q.dense_layers)):
        tQ.dense_layers[dl].w = (Q.dense_layers[dl].w)
        tQ.dense_layers[dl].b = (Q.dense_layers[dl].b)
    tQ.output_layer.w = (tQ.output_layer.w * POLYAK_CONSTANT) + (Q.output_layer.w * (1.0-POLYAK_CONSTANT))
    tQ.output_layer.b = (tQ.output_layer.b * POLYAK_CONSTANT) + (Q.output_layer.b * (1.0-POLYAK_CONSTANT))

    for dl in range(len(P.dense_layers)):
        tP.dense_layers[dl].w = (tP.dense_layers[dl].w * POLYAK_CONSTANT) + (P.dense_layers[dl].w * (1.0-POLYAK_CONSTANT))
        tP.dense_layers[dl].b = (tP.dense_layers[dl].b * POLYAK_CONSTANT) + (P.dense_layers[dl].b * (1.0-POLYAK_CONSTANT))
    tP.output_layer.w = (tP.output_layer.w * POLYAK_CONSTANT) + (P.output_layer.w * (1.0-POLYAK_CONSTANT))
    tP.output_layer.b = (tP.output_layer.b * POLYAK_CONSTANT) + (P.output_layer.b * (1.0-POLYAK_CONSTANT))
    """
    return Q_targ, Q, Policy, Policy_targ

import os
def save_all_networks(Q, tQ, P, tP, model_save_set=0):
    currentdir = os.path.dirname(os.path.realpath(__file__)) # dir to folder holding this file
    dir = currentdir + '/saved_models/model_set_%s' %model_save_set + '/'
    dir_Q = dir + 'Q'
    dir_tQ = dir + 'tQ'
    dir_P = dir + 'P'
    dir_tP = dir + 'tP'
    Q.save_weights(dir_Q)
    tQ.save_weights(dir_tQ)
    P.save_weights(dir_P)
    tP.save_weights(dir_tP)




def depreciated_action_chance(actions_tensor, states_tensor, policy_model, constant=2.5): #sqrt(2pi) ~~ 2.5
    #go through (1/constant) * e^-1((a-mean)^2 / std^2)
    #print("action chance:")
    combined_tensor = policy_model(states_tensor) #outputs a [batch size, output vector size]
    mean, std = tf.split(combined_tensor, 2, axis=1)
    std = tf.math.abs(std)
    action_chance = actions_tensor - mean
    #print(action_chance) #real
    #how high the square of std is compared to the difference makes the lowering of e less, making e higher
    action_chance /= std
    action_chance = tf.math.square(action_chance)
    #print("value to put into exp")
    #print(action_chance) #real
    action_chance = tf.math.exp(action_chance * -0.5)
    #print(action_chance) #real low
    #the lower the std, the higher the chance of each action
    action_chance /= (constant * std)
    #print(action_chance) #real high, and negative which makes log 
    z_score = (actions_tensor-mean) / std #z-score
    #print("zscore: ", z_score)
    action_chance = (1/constant) * tf.math.exp(-0.5 * tf.math.square(z_score))
    #now each individual value in the [output vector size] of [batch size, output vector size] is the probability of that happening, II them together to get chance of action
    action_chance = tf.math.reduce_prod(action_chance, 1) #each [output vector size] reduce_prods within itself
    #print("chances of each action:")
    print(action_chance[0]) #real high
    return action_chance #we multiply by -1 so inputs to log should be < 1